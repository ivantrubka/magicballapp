//
//  ViewController.swift
//  Magic8BallApp
//
//  Created by Ivan Trubka on 19.02.2018.
//  Copyright © 2018 Ivan Trubka. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var randomBallNumber: Int = 0
    
    let ballArray = ["ball1", "ball2", "ball3", "ball4", "ball5"]
    
    @IBOutlet var ballImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ballImageUpdate()
        
    }

    @IBAction func askButtonPressed(_ sender: UIButton) {
        
        ballImageUpdate()
        
    }
    
    func ballImageUpdate(){
        
        randomBallNumber = Int(arc4random_uniform(5))
        
        ballImageView.image = UIImage(named: ballArray[randomBallNumber])
        
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        
        ballImageUpdate()
    }
    
    
}

